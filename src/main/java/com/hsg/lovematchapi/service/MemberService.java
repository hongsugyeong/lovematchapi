package com.hsg.lovematchapi.service;

import com.hsg.lovematchapi.entity.Member;
import com.hsg.lovematchapi.model.member.MemberCreateRequest;
import com.hsg.lovematchapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getData(long id) {
        return memberRepository.findById(id).orElseThrow();
    }

    public void setMember(MemberCreateRequest request) {
        Member addData = new Member();
        addData.setName(request.getName());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setIsMan(request.getIsMan());

        memberRepository.save(addData);
    }
}
