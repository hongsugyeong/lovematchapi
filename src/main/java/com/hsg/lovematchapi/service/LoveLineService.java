package com.hsg.lovematchapi.service;

import com.hsg.lovematchapi.entity.LoveLine;
import com.hsg.lovematchapi.entity.Member;
import com.hsg.lovematchapi.model.loveLine.LoveLineCreateRequest;
import com.hsg.lovematchapi.model.loveLine.LoveLineItem;
import com.hsg.lovematchapi.model.loveLine.LoveLinePhoneNumberChangeRequest;
import com.hsg.lovematchapi.model.loveLine.LoveLineResponse;
import com.hsg.lovematchapi.repository.LoveLineRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class LoveLineService {
    private final LoveLineRepository loveLineRepository;

    public void setLoveLine(Member member, LoveLineCreateRequest request) {
        LoveLine addData = new LoveLine();
        addData.setMember(member);
        addData.setLovePhoneNumber(request.getLovePhoneNumber());

        loveLineRepository.save(addData);
    }

    public List<LoveLineItem> getLoveLines () {
        List<LoveLine> originList = loveLineRepository.findAll();

        List<LoveLineItem> result = new LinkedList<>();

        for(LoveLine loveLine : originList) {
            LoveLineItem addItem = new LoveLineItem();
            addItem.setMemberId(loveLine.getMember().getId());
            addItem.setMemberName(loveLine.getMember().getName());
            addItem.setMemberPhoneNumber(loveLine.getMember().getPhoneNumber());
            addItem.setLovePhoneNumber(loveLine.getLovePhoneNumber());

            result.add(addItem);
        }

        return result;
    }

    public LoveLineResponse getLoveLine (long id) {
        LoveLine detailList = loveLineRepository.findById(id).orElseThrow();

        LoveLineResponse response = new LoveLineResponse();

        response.setId(detailList.getId());
        response.setMemberId(detailList.getMember().getId());
        response.setMemberName(detailList.getMember().getName());
        response.setMemberPhoneNumber(detailList.getMember().getPhoneNumber());
        response.setLovePhoneNumber(detailList.getLovePhoneNumber());

        return response;
    }

    public void  putPhoneNumber (Long id, LoveLinePhoneNumberChangeRequest request) {
        LoveLine originData = loveLineRepository.findById(id).orElseThrow();

        originData.setLovePhoneNumber(request.getLovePhoneNumber());

        loveLineRepository.save(originData);
    }
}
