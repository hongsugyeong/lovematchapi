package com.hsg.lovematchapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoveMatchApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(LoveMatchApiApplication.class, args);
	}

}
