package com.hsg.lovematchapi.controller;

import com.hsg.lovematchapi.model.member.MemberCreateRequest;
import com.hsg.lovematchapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/join")
    public String setMember (@RequestBody MemberCreateRequest request) {
        memberService.setMember(request);

        return "OKK";
    }
}
