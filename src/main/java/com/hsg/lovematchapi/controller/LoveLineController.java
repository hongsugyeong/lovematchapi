package com.hsg.lovematchapi.controller;

import com.hsg.lovematchapi.entity.Member;
import com.hsg.lovematchapi.model.loveLine.LoveLineCreateRequest;
import com.hsg.lovematchapi.model.loveLine.LoveLineItem;
import com.hsg.lovematchapi.model.loveLine.LoveLinePhoneNumberChangeRequest;
import com.hsg.lovematchapi.model.loveLine.LoveLineResponse;
import com.hsg.lovematchapi.service.LoveLineService;
import com.hsg.lovematchapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/love-line")
public class LoveLineController {
    private final MemberService memberService;
    private final LoveLineService loveLineService;

    @PostMapping("/new/member-id/{memberId}")
    public String setLoveLine (@PathVariable long memberId, @RequestBody LoveLineCreateRequest request) {
        Member member = memberService.getData(memberId);
        loveLineService.setLoveLine(member, request);

        return "OKK";
    }

    @GetMapping("/all")
    public List<LoveLineItem> getLoveLines () {
        return loveLineService.getLoveLines();
    }

    @GetMapping("/detail/{id}")
    public LoveLineResponse getLoveLine (@PathVariable long id) {
        return loveLineService.getLoveLine(id);
    }

    @PutMapping("/phoneNumber/love-line-id/{loveLineId}")
    public String putPhoneNumber(@PathVariable long loveLineId, @RequestBody LoveLinePhoneNumberChangeRequest request) {
        loveLineService.putPhoneNumber(loveLineId, request);

        return "OKK";
    }
}
