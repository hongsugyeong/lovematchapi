package com.hsg.lovematchapi.repository;

import com.hsg.lovematchapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Long> {
}
