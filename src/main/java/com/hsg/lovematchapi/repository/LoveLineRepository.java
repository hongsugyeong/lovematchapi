package com.hsg.lovematchapi.repository;

import com.hsg.lovematchapi.entity.LoveLine;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LoveLineRepository extends JpaRepository<LoveLine, Long> {
}
