package com.hsg.lovematchapi.model.loveLine;

import com.hsg.lovematchapi.entity.Member;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoveLineResponse {
    private Long id;
    private Long memberId;
    private String memberName;
    private String memberPhoneNumber;
    private String lovePhoneNumber;
}
